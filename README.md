# Test Map

## Test Map Structure

### Main Map: Wild Match

Right now, there's a main map: [Wild Match](https://platform.wildsky.dev/arcade/game/417), which tries to use a variety of editor features. This map will be further expanded upon to add more coverage for existing features, as well as new editor features. 

The coverage is meant to be broad but not exhaustive. The goal is to test the features in a game development setting, so we could discover potential UI/UX/usability issues not found during the isolated feature testing phase. 

----

### Playground Maps

There will also be various playground maps to test a group of related features (e.g. UI, Engine, etc) in a more isolated setting. Consider adding to the playground if you are working on a feature that can be encapsulated by a separate lua file under the playground map.

#### Adding to the playground map

1. First, add a separate lua file under the `Assets/Scripts` folder.

2. In the separate lua file, add these 2 functions:
    - `SetUpTest()`
    - `FinishTest()`

3. Then in the main `Trigger.lua.txt` file, find a table named `tests`. Add an entry like this: `["Test Name"] = importedTestModule`.

Your test should be ready to go.

----

### Other test maps

If the feature can't be encapsulated by a separate lua file (e.g. presentation script needs a specific file named `Presentation.lua.txt` in the `Assets/Script` folder), you can add it as an extra map.

#### Adding a test map

Create test maps in the `Maps` folder.

1. In the entry map, add (just) the name of the test map to "Maps" in Custom Data tab.

2. In the test map, add `Mods/MapNav` to the local dependency.

3. In the `trigger.lua.txt` file under the test map folder, add these lines:
    ```lua
    local mapNav = GameModules["Mods/MapNav"]

    function OnMapStart()
        mapNav.LoadUIToEntryMap()
    end
    ```

This will list your map in the entry map, and it will handle the navigation back to the entry map.
